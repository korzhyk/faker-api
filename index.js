const path = require('path')
const fg = require('fast-glob')
const express = require('express')
const cors = require('cors')
const app = express()
const port = process.env.PORT || 80

app.use(cors())

fg('api/**/*.js')
.then(routes => routes.map(route => {
	const fileName = path.basename(route, path.extname(route))
	const dirName = path.dirname(route)
	app.get(
		path.join('/', dirName, fileName),
		require(path.join(__dirname, route))
	)
	console.log(path.join('/', dirName, fileName))
}))
.then(() => app.listen(port, () => {
  console.log('API server listening on port', port)
}))
 