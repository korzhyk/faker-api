const faker = require('faker')

module.exports = (req, res) => {
	const firstName = faker.name.firstName()
	const lastName = faker.name.lastName()

	res.json({
		userName: faker.helpers.shuffle([faker.random.number({ max: 999 }), firstName, lastName]).join(''),
		password: faker.internet.password(),
		firstName: firstName,
		lastName: lastName,
		zipCode: faker.address.zipCode(),
		streetName: faker.address.streetAddress(),
		city: faker.address.city(),
		phoneCountry: '1',
		phoneCode: faker.phone.phoneNumber('!##'),
		phoneNumber: faker.phone.phoneNumber('!##-####'),
		countryCode: 'US',
		lang: 'en_US',
		currency: 'USD'
	})
}